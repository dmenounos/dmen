<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib prefix="jwr" uri="http://jawr.net/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tls" uri="http://tiles.apache.org/tags-tiles" %>

<fmt:setBundle basename="memo.view.web.server.messages" scope="session" />
<c:set value="${pageContext.request.contextPath}" var="contextPath" scope="session" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>
			<fmt:message key="title" />
		</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link type="text/css" rel="stylesheet" href="${contextPath}/resources/css/reset.css" />
		<link type="text/css" rel="stylesheet" href="${contextPath}/resources/css/grids.css" />
		<link type="text/css" rel="stylesheet" href="${contextPath}/resources/theme.css" title="Theme" />
		<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Cabin:400,700" />
		<script type="text/javascript" src="${contextPath}/resources/jquery/jquery-1.8.2.js"></script>
		<script type="text/javascript" src="${contextPath}/gwt/gwt.nocache.js"></script>
		<jwr:script src="/bundles/messages.js" />
	</head>
	<body>

		<div id="root">

		<!-- outer-alpha -->

		<div class="headers">
			<div class="container_12">
				<div class="grid_col grid_6">
					<h1>
						<a href="${contextPath}/app/">Dimitris Menounos</a>
					</h1>
					<p class="description">
						<strong>Software design and development</strong>
					</p>
				</div>
				<div class="grid_col grid_6">
					<ul class="menu">
						<li>
							<a href="${contextPath}/app/">About Me</a>
						</li>
						<li>
							<a href="${contextPath}/app/experience">Experience</a>
						</li>
						<%--
						<li>
							<a href="${contextPath}/app/samples">Samples</a>
						</li>
						--%>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<!-- inner-alpha -->

		<div class="content body">
			<div class="container_12">
				<div class="grid_col grid_12">
					<tls:insertAttribute name="content" />
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<!-- inner-omega -->

		<div class="footers">
			<div class="container_12">
				<div class="grid_col grid_12 misc_links">
					<a href="https://docs.google.com/file/d/0B0Vxm5LPHjvBUHhselg5ZEFXZ0U/edit?usp=sharing" target="_blank" title="PDF Resume">
						<img src="${contextPath}/resources/icons/silk/page_white_acrobat.png" alt="Resume" /> PDF Resume
					</a>
					<a href="http://gr.linkedin.com/pub/dimitris-menounos/4/72a/5b8" target="_blank" title="LinkedIn Profile">
						<img src="${contextPath}/resources/icons/social/linkedin_16.png" alt="LinkedIn" /> LinkedIn
					</a>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<!-- outer-omega -->

		</div>

		<iframe src="javascript:''" id="__gwt_historyFrame" style="display: none;"></iframe>

	</body>
</html>
