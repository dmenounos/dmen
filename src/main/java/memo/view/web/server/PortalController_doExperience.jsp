<div class="experience alpha">

	<h2>Professional Experience</h2>

	<table>
		<tr>
			<th>
				<p>Period:</p>
			</th>
			<td class="east_col">
				<p>February 2010 - Today</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Employer:</p>
			</th>
			<td class="east_col">
				<p>Avaca Technologies</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Occupation:</p>
			</th>
			<td class="east_col">
				<p>Development of data services and user interfaces for several enterprise applications.</p>
				<p>Moreover I have contributed heavily in the creation of a Wicket / ExtJS <a href="http://avaca.eu:8080/javaca-wxt-docs-1.0/app/?source=true" target="_blank" class="external">bridge</a> that helps writing UI components in server side Java and have them automatically rendered as client side JavaScript.</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Projects:</p>
			</th>
			<td class="east_col">
				<p><a href="${contextPath}/samples/screens/2010/allianz.png" rel="popup">ALLIANZ DIRECT</a> (<a href="http://www.allianzdirect.gr/" target="_blank" class="external">site</a>)<br />
				The project's scope was to develop a web application that would allow customers to fulfill on-line insurance requests, a set of provided services involving the whole insurance process management and respectively the relevant administration tools.<br />
				Technologies used: Spring, Apache Wicket, ExtJS, SQLServer, Tomcat</p>
				<p><a href="${contextPath}/samples/screens/2010/villas.png" rel="popup">Beyond Spaces Villas</a> (<a href="http://www.beyondspacesvillas.com/" target="_blank" class="external">site</a>)<br />
				The project's scope was to develop a web application that would allow live on-line renting requests by finding the perfect Greek Island luxury villa and ensuring that Greek island holiday experience is unique.<br />
				Technologies used: Spring, Apache Wicket, ExtJS, PostgreSQL, Tomcat</p>
				<p><a href="${contextPath}/samples/screens/2010/titan.png" rel="popup">TITAN IR</a> (<a href="http://ir.titan.gr/" target="_blank" class="external">site</a>)<br />
				The project's scope was to develop a web application that would allow content management as well as portal presentation for the investor relations TITAN division.<br />
				Technologies used: Spring, Apache Wicket, ExtJS, PostgreSQL, Tomcat</p>
				<p><a href="${contextPath}/samples/screens/2010/korres.png" rel="popup">KORRES IR</a> (<a href="http://ir.korres.com/" target="_blank" class="external">site</a>)<br />
				The project's scope was to develop a web application that would allow content management as well as portal presentation for the investor relations KORRES division.<br />
				Technologies used: Spring, Apache Wicket, ExtJS, PostgreSQL, Tomcat</p>
				<p><a href="${contextPath}/samples/screens/2010/pescos.jpg" rel="popup">PESCOS</a> (<a href="http://www.pescos.eu/" target="_blank" class="external">site</a>)<br />
				The project's scope was to develop a web application that would allow content management as well as portal presentation for the PESCOS European project.
				Technologies used: Spring, Apache Wicket, ExtJS, PostgreSQL, Tomcat</p>
				<p><a href="${contextPath}/samples/screens/2010/trader.jpg" rel="popup">Eurobank Trader</a> (<a href="http://www.eurobanktrader.gr/" target="_blank" class="external">site</a>)<br />
				The project's scope was to develop a web application with restricted access that could allow live on-line stock market transactions. It was a multilingual application (Greek, English, Romanian) based on latest GWT technology.<br />
				Technologies used: EJB, GWT, GXT, CSS, JavaScript, JBoss</p>
			</td>
		</tr>
	</table>

	<table>
		<tr>
			<th>
				<p>Period:</p>
			</th>
			<td class="east_col">
				<p>August 2007 - August 2009</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Employer:</p>
			</th>
			<td class="east_col">
				<p>Quality &amp; Reliability - Konitsis 11B, Marousi</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Occupation:</p>
			</th>
			<td class="east_col">
				<p>Development of Java EE based information systems.</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Projects:</p>
			</th>
			<td class="east_col">
				<ul>
					<li>
						<p>National Telecommunications and Post Commission (EETT)<br />
					</li>
					<li>
						<p>Greek Culture Organization (OPEP) - "Odysseas"<br />
					</li>
					<li>
						<p>Regulator Authority for Energy (RAE)<br />
					</li>
					<li>
						<p>Q&amp;R CMS<br />
					</li>
				</ul>
				<p>My role involved mainly transitioning existing content management system user interfaces from legacy technologies, such as Apache Struts, to modern AJAX based using the ExtJS framework.</p>
				<p>Technologies used: Java EE, DWR, JSP, ExtJS, Oracle</p>
			</td>
		</tr>
	</table>

	<table>
		<tr>
			<th>
				<p>Period:</p>
			</th>
			<td class="east_col">
				<p>September 2003 - June 2007</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Employer:</p>
			</th>
			<td class="east_col">
				<p>PaperSoft - Syggrou Avenue 249, Athens</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Occupation:</p>
			</th>
			<td class="east_col">
				<p>My role was to develop new features and tools as well as maintain existing code for an existing product that allowed thorough analysis of the commercial advertising and indirect presentations that went in the press.</p>
				<p>Technologies used: Java SE, Lotus Notes / Domino</p>
			</td>
		</tr>
	</table>

	<table>
		<tr>
			<th>
				<p>Period:</p>
			</th>
			<td class="east_col">
				<p>February 2002 - August 2003</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Employer:</p>
			</th>
			<td class="east_col">
				<p>DV Interpower - 3rd September, Athens</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Occupation:</p>
			</th>
			<td class="east_col">
				<p>Design and implementation of several small business web pages.<br />
				Technologies used: PHP, HTML, CSS, JavaScript</p>
			</td>
		</tr>
	</table>

	<table>
		<tr>
			<th>
				<p>Period:</p>
			</th>
			<td class="east_col">
				<p>May 2001 - November 2001</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Employer:</p>
			</th>
			<td class="east_col">
				<p>Hartling Group, Athens</p>
			</td>
		</tr>
		<tr>
			<th>
				<p>Occupation:</p>
			</th>
			<td class="east_col">
				<p>Development of medical focused software, such as a database of medical law and websites promoting voluntary blood donation.<br />
				Technologies used: HTML, CSS, JavaScript</p>
			</td>
		</tr>
	</table>

</div>
