<div class="samples">

	<h2>Recent Work Samples</h2>
	<div id="gallery-2">
		<img src="${contextPath}/samples/screens/2010/extjs.jpg" alt="" />
		<img src="${contextPath}/samples/screens/2010/pescos.jpg" alt="" />
		<img src="${contextPath}/samples/screens/2010/titan.jpg" alt="" />
		<img src="${contextPath}/samples/screens/2010/avaca.jpg" alt="" />
	</div>

	<h2>Earlier Work Samples</h2>
	<%--
	<ul>
		<li><a href="${contextPath}/samples/sites/keimeno/index.htm" target="_blank">Κείμενο</a></li>
		<li><a href="${contextPath}/samples/sites/ecopaper/index.htm" target="_blank">Ecopaper</a></li>
		<li><a href="${contextPath}/samples/sites/tassotti/index.htm" target="_blank">Tassotti</a></li>
	</ul>
	--%>
	<div id="gallery-1">
		<img src="${contextPath}/samples/screens/2002/keimeno.jpg" alt="" />
		<img src="${contextPath}/samples/screens/2002/gavalas.jpg" alt="" />
		<img src="${contextPath}/samples/screens/2002/egglezakis.jpg" alt="" />
		<img src="${contextPath}/samples/screens/2002/skoutaris.jpg" alt="" />
		<img src="${contextPath}/samples/screens/2002/kglaw.jpg" alt="" />
		<img src="${contextPath}/samples/screens/2002/drpapami.jpg" alt="" />
		<%--
		<img src="${contextPath}/samples/screens/2002/katramatou.jpg" alt="" />
		<img src="${contextPath}/samples/screens/2002/gypsotexniki.jpg" alt="" />
		<img src="${contextPath}/samples/screens/2002/special.jpg" alt="" />
		--%>
	</div>

</div>

<%--
<script type="text/javascript">
$(function() {
	alert('koko');
});
</script>
--%>
