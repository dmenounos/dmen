<div class="introduction alpha">

	<h2>Introduction</h2>
	<p>I am a software developer from Athens, Greece.</p>
	<p>I am proficient in the <a href="http://www.oracle.com/us/technologies/java/index.html" target="_blank" class="external">Java</a> programming language, as well as in established frameworks that accompany the platform such as <a href="http://www.springsource.org/" target="_blank" class="external">Spring</a>, <a href="http://www.hibernate.org/" target="_blank" class="external">Hibernate</a>, etc. I have good knowledge of standards such as HTML, XML, CSS, JavaScript and extensive experience in client-side programming with AJAX toolkits such as <a href="http://www.sencha.com/products/extjs/" target="_blank" class="external">ExtJS</a> and <a href="http://code.google.com/webtoolkit/" target="_blank" class="external">GWT</a>.</p>
	<p>So far I have gained familiarity in a variety of software development environments and tools, though I am always interested in expanding and improving my knowledge.</p>

</div>

<div class="contact omega">

	<h2>Contact</h2>
	<dl class="details">
		<dt>
			<span>Email:</span>
		</dt>
		<dd>
			<p>dmenounos@gmail.com</p>
			<p>dmenounos@yahoo.com</p>
		</dd>
		<dt>
			<span>Phone:</span>
		</dt>
		<dd>
			<p>697 4384142</p>
			<p class="del">693 7460574</p>
		</dd>
	</dl>

</div>
