/*
 * Copyright (C) 2010 Dimitrios Menounos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package memo.view.web.server;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PortalController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView doRoot() {
		return new ModelAndView("/");
	}

	@RequestMapping(value = "/experience", method = RequestMethod.GET)
	public ModelAndView doExperience() {
		sleepThread(500); // add some delay
		return new ModelAndView("/experience");
	}

	@RequestMapping(value = "/samples.pro", method = RequestMethod.GET)
	public ModelAndView doSamples() {
		sleepThread(500); // add some delay
		return new ModelAndView("/samples");
	}

	protected void sleepThread(int time) {
		try {
			Thread.sleep(time);
		}
		catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
}
